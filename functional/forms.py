from django import forms
from django.forms import widgets

class StatusForm(forms.Form):
	msg = forms.CharField(widget=forms.TextInput(attrs={"id":"status", "maxlength":"300"}))
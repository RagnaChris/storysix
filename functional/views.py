from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

# Create your views here.
def uji(request):
	if request.method == 'POST':
		form = StatusForm(request.POST)
		if form.is_valid():
			m = form.cleaned_data['msg']
			s = Status(msg=m)
			s.save()
			return redirect('uji')			
	else:
		form = StatusForm()
	status = Status.objects.order_by("-date")
	return render(request, 'uji.html', {'form': form, "status": status})
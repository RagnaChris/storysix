from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from .views import *
from .models import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import chromedriver_binary

# Create your tests here.
class Story6Test(TestCase):
	def test_uji_is_exist(self):
		response = Client().get('/uji/')
		self.assertEqual(response.status_code, 200)

	def test_uji_using_uji_template(self):
		response = Client().get('/uji/')
		self.assertTemplateUsed(response, 'uji.html')

	def test_uji_using_uji_func(self):
		found = resolve('/uji/')
		self.assertEqual(found.func, uji)

	def test_models_can_create_status(self):
		s = Status.objects.create(msg="Coba Coba", date=timezone.now())
		counting = Status.objects.count()
		self.assertEqual(counting, 1)

	def test_can_save_a_POST_request(self):
		response = Client().post('/uji/', data={'msg': 'Coba Coba', 'date':'2019-10-28T16:00'})
		counting = Status.objects.count()
		self.assertEqual(counting, 1)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/uji/')
		more = Client().get('/uji/')
		html_response = more.content.decode('utf8')
		self.assertIn('Coba Coba', html_response)

class NewStatusTest(unittest.TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser = webdriver.Chrome(chrome_options=chrome_options)

	def tearDown(self):
		self.browser.quit()

	def test_can_post_status_and_find_it(self):
		self.browser.get('http://localhost:8000/uji')
		msg = self.browser.find_element_by_id('status')
		send = self.browser.find_element_by_id('send')
		msg.send_keys('Coba Coba')
		send.send_keys(Keys.RETURN)
		results = self.browser.find_element_by_class_name('stats').text
		self.assertIn('Coba Coba', results)

	def test_300_max_length(self):
		self.browser.get('http://localhost:8000/uji')
		msg = self.browser.find_element_by_id('status')
		send = self.browser.find_element_by_id('send')
		msg.send_keys('1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901')
		send.send_keys(Keys.RETURN)
		results = self.browser.find_element_by_class_name('stats').text
		self.maxDiff = None
		self.assertEqual(len(results), 300)